<?php
declare(strict_types=1);
/**
 * This file is part of Teddy Framework.
 *
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2022-03-25 11:37:46 +0800
 */

return [
    new App\Commands\ConfigCommand(),
];
