<?php
declare(strict_types=1);
/**
 * This file is part of Teddy Framework.
 *
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2021-09-03 11:37:54 +0800
 */

namespace Teddy\Flysystem\Adapters;

use Overtrue\Flysystem\Cos\CosAdapter as OvertrueCosAdapter;

class CosAdapter extends OvertrueCosAdapter
{
}
