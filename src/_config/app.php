<?php
declare(strict_types=1);
/**
 * This file is part of Teddy Framework.
 *
 * @author   Fung Wing Kit <wengee@gmail.com>
 * @version  2022-03-29 16:39:40 +0800
 */

return [
    'name'     => 'Teddy App',
    'version'  => '1.0.0',
    'server'   => php_uname('n'),
    'timezone' => 'Asia/Shanghai',

    'urlPrefix' => '',
];
